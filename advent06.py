from collections import defaultdict

from copy import copy
lights = defaultdict(bool)
test_lights = defaultdict(bool)

def off(p1, p2, lights):
    for x in range(p1[0], p2[0]+1):
        for y in range(p1[1], p2[1]+1):
            if (x, y) in lights:
                del lights[(x, y)]
    return lights

off_test_lights = defaultdict(bool)
off_test_lights[(2,3)] = True
off_test_lights[(5,5)] = True

assert len(off_test_lights) == 2
off([2,3], [2,4], off_test_lights)
assert len(off_test_lights) == 1


def on(p1, p2, lights):
    for x in range(p1[0], p2[0]+1):
        for y in range(p1[1], p2[1]+1):
            lights[(x, y)] = True
    return lights

on_test_lights = defaultdict(bool)
on_test_lights[(0,0)] = True
on_test_lights[(9,9)] = True

assert len(on_test_lights) == 2
on([0,0], [1,1], on_test_lights)
assert len(on_test_lights) == 5

def toggle(p1, p2, lights):
    for x in range(p1[0], p2[0]+1):
        for y in range(p1[1], p2[1]+1):
            if (x, y) in lights:
                del lights[(x, y)]
            else:
                lights[(x, y)] = True
    return lights

toggle_test_lights = defaultdict(bool)
toggle_test_lights[(0,0)] = True
toggle_test_lights[(2,2)] = True

assert len(toggle_test_lights) == 2
toggle([0,0], [2,2], toggle_test_lights)
assert len(toggle_test_lights) == 7

def parse_line(line):
    if line.startswith('turn off'):
        action = off
        line = line.replace('turn off ', '')
    elif line.startswith('turn on'):
        action = on
        line = line.replace('turn on ', '')
    elif line.startswith('toggle'):
        action = toggle
        line = line.replace('toggle ', '')
    
    points = line.split(' through ')
    p1 = [int(coord) for coord in points[0].split(',')]
    p2 = [int(coord) for coord in points[1].split(',')]
    
    return action, p1, p2

assert parse_line('toggle 760,127 through 829,189') == (toggle, [760,127], [829,189])

fh = open('input6.txt', 'r')

for line in fh:
    action, start, finish = parse_line(line)
    
    action(start, finish, lights)

print len(lights)


### New translation
new_lights = defaultdict(int)

def new_off(p1, p2, lights):
    for x in range(p1[0], p2[0]+1):
        for y in range(p1[1], p2[1]+1):
            lights[(x, y)] = max(0, lights[(x, y)] - 1)
    return lights

def new_on(p1, p2, lights):
    for x in range(p1[0], p2[0]+1):
        for y in range(p1[1], p2[1]+1):
            lights[(x, y)] += 1
    return lights

def new_toggle(p1, p2, lights):
    for x in range(p1[0], p2[0]+1):
        for y in range(p1[1], p2[1]+1):
            lights[(x, y)] += 2
    return lights

def new_parse_line(line):
    if line.startswith('turn off'):
        action = new_off
        line = line.replace('turn off ', '')
    elif line.startswith('turn on'):
        action = new_on
        line = line.replace('turn on ', '')
    elif line.startswith('toggle'):
        action = new_toggle
        line = line.replace('toggle ', '')
    
    points = line.split(' through ')
    p1 = [int(coord) for coord in points[0].split(',')]
    p2 = [int(coord) for coord in points[1].split(',')]
    
    return action, p1, p2
        
def get_total_brightness(lights):
    return sum(lights.values())


fh.seek(0)
for line in fh:
    action, p1, p2 = new_parse_line(line)
    action(p1, p2, new_lights)

print get_total_brightness(new_lights)
