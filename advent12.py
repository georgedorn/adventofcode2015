def find_numbers(data):
    """
    Recursive algorithm to traverse a JSON data structure
    and return all integers.
    """
    if isinstance(data, int):
        yield data
    elif isinstance(data, list):
        for item in data:
            for res in find_numbers(item):
                yield res
    elif isinstance(data, dict):
        for k, v in data.iteritems():
            for res in find_numbers(v):
                yield res

def sum_numbers(data):
    return sum([i for i in find_numbers(data)])
    
#tests
assert sum_numbers([1,2,3]) == 6
assert sum_numbers({"a":2,"b":4}) == 6
assert sum_numbers([[[3]]]) == 3
assert sum_numbers({"a":{"b":4},"c":-1}) == 3
assert sum_numbers({"a":[-1,1]}) == 0
assert sum_numbers([-1,{"a":1}]) == 0
assert sum_numbers([]) == 0
assert sum_numbers({}) == 0

import json
with open('input12.txt') as fh:
    data = json.load(fh)

print "Part 1:", sum_numbers(data)


# Part 2:
def find_nonred_numbers(data):
    """
    Recursive algorithm to traverse a JSON data structure
    and return all integers.
    """
    if isinstance(data, int):
        yield data
    elif isinstance(data, list):
        for item in data:
            for res in find_nonred_numbers(item):
                yield res
    elif isinstance(data, dict):
        # this is our special case
        if 'red' not in data.values():
            for k, v in data.iteritems():
                for res in find_nonred_numbers(v):
                    yield res

def sum_nonred_numbers(data):
    return sum([i for i in find_nonred_numbers(data)])

#tests
assert sum_nonred_numbers([1,{"c":"red","b":2},3]) == 4


print "Part 2:", sum_nonred_numbers(data)

