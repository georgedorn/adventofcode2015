from itertools import combinations_with_replacement
from collections import namedtuple, defaultdict

class Recipe(object):
    
    def __init__(self):
        self.capacity = 0
        self.durability = 0
        self.flavor = 0
        self.texture = 0
        self.calories = 0
        
        self.ingredients = defaultdict(int)
    
    @property
    def score(self):
        return ( max(0, self.capacity) 
                *max(0, self.durability)
                *max(0, self.flavor)
                *max(0, self.texture) )
    
    def add_ingredient(self, ingredient):
        self.capacity += ingredient.capacity
        self.durability += ingredient.durability
        self.flavor += ingredient.flavor
        self.texture += ingredient.texture
        self.calories += ingredient.calories
        self.ingredients[ingredient] += 1

    def add_multiple(self, ingredient, amount):
        self.capacity += ingredient.capacity * amount
        self.durability += ingredient.durability * amount
        self.flavor += ingredient.flavor * amount
        self.texture += ingredient.texture * amount
        self.calories += ingredient.calories * amount
        self.ingredients[ingredient] += amount


Ingredient = namedtuple('Ingredient', ['capacity', 'durability', 'flavor', 'texture', 'calories'])


#testing
butterscotch = Ingredient(capacity=-1, durability=-2, flavor=6, texture=3, calories=8)
cinnamon = Ingredient(capacity=2, durability=3, flavor=-2, texture=-1, calories=3)

test_recipe = Recipe()
test_recipe.add_multiple(butterscotch, 44)
test_recipe.add_multiple(cinnamon, 56)
assert test_recipe.score == 62842880


#input:
#Sugar: capacity 3, durability 0, flavor 0, texture -3, calories 2
sugar = Ingredient(capacity=3, durability=0, flavor=0, texture=-3, calories=2)
#Sprinkles: capacity -3, durability 3, flavor 0, texture 0, calories 9
sprinkles = Ingredient(capacity=-3, durability=3, flavor=0, texture=0, calories=9)
#Candy: capacity -1, durability 0, flavor 4, texture 0, calories 1
candy = Ingredient(capacity=-1, durability=0, flavor=4, texture=0, calories=1)
#Chocolate: capacity 0, durability 0, flavor -2, texture 2, calories 8
chocolate = Ingredient(capacity=0, durability=0, flavor=-2, texture=2, calories=8)

ingredients = [sugar, sprinkles, candy, chocolate]

best_recipe = None
best_score = None
best_score_500_calories = None
best_recipe_500_calories = None
for combo in combinations_with_replacement(ingredients, 100):
    recipe = Recipe()
    for ingredient in combo:
        recipe.add_ingredient(ingredient)
    score = recipe.score
    if best_score is None or score > best_score:
        best_score = score
        best_recipe = recipe
    if recipe.calories == 500:
        if score > best_score_500_calories:
            best_score_500_calories = score
            best_recipe_500_calories = recipe


print "Part one:", best_score
print "Part two:", best_score_500_calories, best_recipe_500_calories.ingredients, best_recipe_500_calories.calories
