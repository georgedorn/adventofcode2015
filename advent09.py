from itertools import permutations
from copy import copy

seen_cities = set()
distances = dict()

def _make_key(*cities):
    return "%s---%s" % (cities[0], cities[1])

def add_line(line):
    parts = line.split(' ')
    cities = (parts[0], parts[2])
    distance = int(parts[4])

    seen_cities.update(cities)

    # Add both directions
    distances[_make_key(*cities)] = distance

# test
test_input = """London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141"""
for line in test_input.split("\n"):
    add_line(line)

assert seen_cities == set(['London', 'Belfast', 'Dublin'])
assert distances == {'London---Belfast': 518, 'Dublin---Belfast': 141, 'London---Dublin': 464}

def get_route_distance(route):
    """
    Given a route of cities (by name) find the total distance.
    """
    tmp_route = list(copy(route))
    total_distance = 0
    while len(tmp_route) > 1:
        start = tmp_route.pop(0)
        finish = tmp_route[0] # don't pop it
        try:
            distance = distances[_make_key(start, finish)]
        except KeyError:
            distance = distances[_make_key(finish, start)]
        total_distance += distance
    return total_distance

def find_best_route(cities):
    best = None
    best_route = None
    for route in permutations(cities):
        distance = get_route_distance(route)
        if best is None or distance < best:
            best = distance
            best_route = route
    return best_route, best

def find_worst_route(cities):
    worst = None
    worst_route = None
    for route in permutations(cities):
        distance = get_route_distance(route)
        if worst is None or distance > worst:
            worst = distance
            worst_route = route
    return worst_route, worst


# route tests
test_route = ['Dublin', 'London', 'Belfast']
assert get_route_distance(test_route) == 982
test_route = ['London', 'Dublin', 'Belfast']
assert get_route_distance(test_route) == 605
 
# test finding best route
best_route, best_distance = find_best_route(seen_cities)
assert best_route == ('London', 'Dublin', 'Belfast')
assert best_distance == 605

# Puzzle input

seen_cities = set()
distances = dict()

for line in open('input9.txt'):
    add_line(line.strip())

print find_best_route(seen_cities)
print find_worst_route(seen_cities)


