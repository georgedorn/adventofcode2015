fh = open('input3.txt', 'r')
dirs = fh.read()
fh.close()

x = 0
y = 0
houses = dict()

housename = '(%s, %s)' % (x, y)
houses[housename] = True

for dir in dirs:
    if dir == '^':
        y -= 1
    elif dir == 'v':
        y += 1
    elif dir == '<':
        x -= 1
    elif dir == '>':
        x += 1
    
    housename = '(%s, %s)' % (x, y)
    houses[housename] = True

print len(houses)


next_year_houses = dict()
next_year_houses['(0, 0)'] = True
positions = [[0,0],[0,0]]

turn = 0

#dirs = '^>v<'


for dir in dirs:
    position = positions[turn]
    print "%s is at %s and moving %s" % (turn, position, dir)

    if dir == '^':
        position[0] -= 1
    elif dir == 'v':
        position[0] += 1
    elif dir == '<':
        position[1] -= 1
    elif dir == '>':
        position[1] += 1
    
    
    housename = '(%s, %s)' % (position[0], position[1])
    next_year_houses[housename] = True
    positions[turn] = position
    turn = (turn + 1) % 2

#print next_year_houses

print len(next_year_houses)
    
