import string

def password_valid(password):
    if 'i' in password or 'o' in password or 'l' in password:
        return False
    
    pair_count = 0
    for letter in string.lowercase:
        if '%s%s' % (letter, letter) in password:
            pair_count += 1
        if pair_count >= 2:
            break #can stop looking
    if pair_count < 2:
        return False
    
    for letter in list(string.lowercase)[:-2]:
        expected = letter + chr(ord(letter)+1) + chr(ord(letter)+2)
        if expected in password:
            return True
    
    return False

#tests
assert not password_valid('hijklmmn')
assert not password_valid('abbceffg')
assert not password_valid('abbcegjk')

assert password_valid('ghjaabcc')
assert password_valid('abcdffaa')

def pass_to_nums(password):
    return [ord(l) for l in password]

def nums_to_pass(nums):
    return ''.join([chr(n) for n in nums])

def increment_password(password):
    #adds one letter to the last letter, rolling over to a and
    #carrying one as needed.
    
    #recursion base case.
    # we don't carry a 1 from the leftmost column because
    # that would increase the length of the password.
    # so instead this just causes z to roll over to a
    if password == '':
        return ''  
    
    ords = pass_to_nums(password)
    ords = [ord(l) for l in password]
    ords[-1] += 1
    if ords[-1] > ord('z'):
        # recursively increment the rest of the password
        # due to carrying a 1 
        return increment_password(nums_to_pass(ords[:-1])) + 'a'
    else:
        return nums_to_pass(ords)

assert increment_password('aaa') == 'aab'
assert increment_password('aaz') == 'aba'

def jump_ahead_increment(password):
    # tiny optimization:  if an invalid letter exists, increment it
    # and set everything to the right to 'a'
    for bad_letter in ['i', 'o', 'l']:
        if bad_letter in password:
            stem, tail = password.split(bad_letter, 1) # only split on first
            password = stem + chr(ord(bad_letter)+1) + 'a'*len(tail)
            assert len(password) == 8
    return password


def get_next_valid_password(password):
    password = jump_ahead_increment(password)
    password = increment_password(password)
    while not password_valid(password):
        password = increment_password(password)
    return password

assert get_next_valid_password('abcdefgh') == 'abcdffaa'
assert get_next_valid_password('ghijklmn') == 'ghjaabcc'

print "Assertions passed, moving on."
password = 'vzbxkghb'

next_password = get_next_valid_password(password)
print next_password

print get_next_valid_password(next_password)
