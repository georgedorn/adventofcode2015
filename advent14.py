#Dancer can fly 27 km/s for 5 seconds, but then must rest for 132 seconds.
#Cupid can fly 22 km/s for 2 seconds, but then must rest for 41 seconds.
#Rudolph can fly 11 km/s for 5 seconds, but then must rest for 48 seconds.
#Donner can fly 28 km/s for 5 seconds, but then must rest for 134 seconds.
#Dasher can fly 4 km/s for 16 seconds, but then must rest for 55 seconds.
#Blitzen can fly 14 km/s for 3 seconds, but then must rest for 38 seconds.
#Prancer can fly 3 km/s for 21 seconds, but then must rest for 40 seconds.
#Comet can fly 18 km/s for 6 seconds, but then must rest for 103 seconds.
#Vixen can fly 18 km/s for 5 seconds, but then must rest for 84 seconds.

class Reindeer(object):
    def __init__(self, speed, endurance, rest_period):
        self.speed = speed
        self.endurance = endurance
        self.rest_period = rest_period
        
        self.points = 0
        
        #tracking vars
        self._remaining_endurance = self.endurance
        self._rest_needed = 0 #seconds
        self._distance_travelled = 0

    def reset(self):
        self.points = 0
        self._remaining_endurance = self.endurance
        self._rest_needed = 0 #seconds
        self._distance_travelled = 0
    
    def tick(self):
        if self._remaining_endurance > 0:
            self._remaining_endurance -= 1
            self._distance_travelled += self.speed
            if self._remaining_endurance == 0:
                self._rest_needed = self.rest_period
        elif self._rest_needed > 0:
            self._rest_needed -= 1
            if self._rest_needed == 0:
                self._remaining_endurance = self.endurance

    def race(self, duration):
        self.reset()

        for i in range(duration):
            self.tick()
        return self.distance

    @property
    def distance(self):
        return self._distance_travelled

    @property
    def is_resting(self):
        return self._rest_needed > 0

# Tests
comet = Reindeer(speed=14, endurance=10, rest_period=127)
dancer = Reindeer(speed=16, endurance=11, rest_period=162)

assert comet.race(1) == 14
assert dancer.race(1) == 16
assert comet.race(10) == 140
assert dancer.race(10) == 160
assert comet.race(11) == 140
#and comet is resting:
assert comet.is_resting
assert dancer.race(11) == 176
assert comet.race(12) == 140
assert dancer.race(12) == 176
assert comet.race(148) == 280
assert dancer.race(185) == 352

#and the big race
assert comet.race(1000) == 1120
assert comet.is_resting
assert dancer.race(1000) == 1056
assert dancer.is_resting


deer = []
deer.append(Reindeer(speed=27, endurance=5, rest_period=132)) # Dancer
deer.append(Reindeer(speed=22, endurance=2, rest_period=41)) # Cupid
deer.append(Reindeer(speed=11, endurance=5, rest_period=48)) # Rudolph
deer.append(Reindeer(speed=28, endurance=5, rest_period=134)) # Donner
deer.append(Reindeer(speed=4, endurance=16, rest_period=55)) # Dasher
deer.append(Reindeer(speed=14, endurance=3, rest_period=38)) # Blitzen
deer.append(Reindeer(speed=3, endurance=21, rest_period=40)) # Prancer
deer.append(Reindeer(speed=18, endurance=6, rest_period=103)) # Comet
deer.append(Reindeer(speed=18, endurance=5, rest_period=84)) # Vixen

distances = []
for d in deer:
    distances.append(d.race(2503))

distances.sort()
print "Part one:", distances[-1]

[d.reset() for d in deer]

# Part two:  one tick at a time

def race_deer(deer, duration):
    [d.reset() for d in deer]
    for i in range(duration):
        [d.tick() for d in deer]
        deer.sort(key=lambda d: d.distance)
        best = deer[-1].distance
        
        # Give a point to all deer at this distance, in case of ties
        for d in deer:
            if d.distance == best:
                d.points += 1
    return sorted(deer, key=lambda d: d.points)

#tests
test_deer = [comet, dancer]
race_deer(test_deer, 1)
assert dancer.points == 1
race_deer(test_deer, 140)
assert dancer.points == 139
assert comet.points == 1
race_deer(test_deer, 1000)
assert dancer.points == 689
assert comet.points == 312


#actual data
deer = race_deer(deer, 2503)
print "Part two:", deer[-1].points




