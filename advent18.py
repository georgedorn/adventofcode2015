# Create lights:


def load_lights(lines):
    lights = dict()
    y = 0
    for line in lines:
        for x, char in enumerate(line):
            if char == '#':
                lights[(x,y)] = True
            elif char == '.':
                lights[(x,y)] = False
        y += 1
    return lights



def neighbor_count(x, y, lights):
    total_on = 0
    for point in [(x-1, y-1),
                  (x-1, y),
                  (x-1, y+1),
                  (x, y-1),
                  (x, y+1),
                  (x+1, y-1),
                  (x+1, y),
                  (x+1, y+1)]:
        if lights.get(point):
            total_on += 1
    return total_on

def find_corners(lights):
    points = lights.keys()
    points.sort(key=lambda p: p[0]) # sort by x
    min_x = points[0][0]
    max_x = points[-1][0]
    points.sort(key=lambda p: p[1]) # sort by y
    min_y = points[0][1]
    max_y = points[-1][1]
    corners = [(min_x, min_y), (min_x, max_y), (max_x, min_y), (max_x, max_y)]
    return corners

def update_lights(lights, corners_stuck=False):
    new_lights = dict()
    if corners_stuck:
        corners = find_corners(lights)
        for corner in corners:
            new_lights[corner] = True
            lights[corner] = True # in case input is missing these stuck lights

    for point, state in lights.iteritems():
        neighbors = neighbor_count(point[0], point[1], lights)
        if state: # light is on
            if neighbors in (2,3):
                new_lights[point] = True
            else:
                new_lights[point] = False
        else:
            if neighbors == 3:
                new_lights[point] = True
            else:
                new_lights[point] = False

    if corners_stuck:
        corners = find_corners(lights)
        for corner in corners:
            new_lights[corner] = True
            lights[corner] = True # in case input is missing these stuck lights

    return new_lights

def print_lights(lights, width, height):
    for y in range(height):
        line = ''
        for x in range(width):
            if lights[(x,y)]:
                line += '#'
            else:
                line += '.'
        print line

def count_lights_on(lights):
    return len([v for v in lights.values() if v])

#testing
test_input = """.#.#.#
...##.
#....#
..#...
#.#..#
####.."""
test_lights = load_lights(test_input.split("\n"))
assert len(test_lights) == 6 * 6
assert count_lights_on(test_lights) == test_input.count('#')

#step 1
test_lights = update_lights(test_lights)
assert count_lights_on(test_lights) == 11

#step 2
test_lights = update_lights(test_lights)
#step 3
test_lights = update_lights(test_lights)
#step 4
test_lights = update_lights(test_lights)
assert count_lights_on(test_lights) == 4

# and these specific lights
for k in test_lights:
    if k in [(2,2), (2,3), (3,2), (3,3)]:
        assert test_lights[k]
    else:
        assert not test_lights[k]


# Actual puzzle
lights = load_lights(open('input18.txt'))

assert len(lights) == 100 * 100

for i in range(100):
    lights = update_lights(lights)

print "Part one:", count_lights_on(lights)

# Part two, stuck corners
# tests
test_lights = load_lights(test_input.split("\n"))
for i in range(5):
    test_lights = update_lights(test_lights, corners_stuck=True)
assert count_lights_on(test_lights) == 17



lights = load_lights(open('input18.txt'))
for i in range(100):
    lights = update_lights(lights, corners_stuck=True)

print "Part two:", count_lights_on(lights)
