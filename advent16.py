sues = []

for line in open('input16.txt'):
    line = line.strip()
    #Sue 1: goldfish: 9, cars: 0, samoyeds: 9
    parts = line.split(' ')
    sue = {}
    sue['number'] = parts[1].rstrip(':')
    parts.pop(0) # Sue
    parts.pop(0) # number
    while parts:
        key = parts.pop(0).rstrip(':')
        val = parts.pop(0).rstrip(',')
        sue[key] = int(val)
    sues.append(sue)

data = {'children': 3,
        'cats': 7,
        'samoyeds': 2,
        'pomeranians': 3,
        'akitas': 0,
        'vizslas': 0,
        'goldfish': 5,
        'trees': 3,
        'cars': 2,
        'perfumes': 1,
    }

def is_valid_sue(sue, data):
    for k,v in sue.items():
        if k in data and data[k] != v:
            return False
    return True

def is_valid_sue_retro(sue, data):
    for k,v in sue.items():
        if k in ('cats', 'trees'):
            if k in data and data[k] >= v:
                return False
        elif k in ('pomeranians', 'goldfish'):
            if k in data and data[k] <= v:
                return False
        elif k in data and data[k] != v:
            return False
    return True

for sue in sues:
    if is_valid_sue(sue, data):
        print "Part one:", sue['number']
        break

for sue in sues:
    if is_valid_sue_retro(sue, data):
        print "Part two:", sue['number']
        break
