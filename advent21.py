from collections import namedtuple

Item = namedtuple('Item', ['name', 'cost', 'attack', 'defense'])

weapons = [ 
#            Item('none', 0, 0, 0),  # MUST buy a weapon according to the rules
            Item('dagger', 8, 4, 0),
            Item('shortsword', 10, 5, 0),
            Item('warhammer', 25, 6, 0),
            Item('longsword', 40, 7, 0),
            Item('greataxe', 74, 8, 0)
        ]

armors = [
          Item('none', 0, 0, 0),
          Item('leather', 13, 0, 1),
          Item('chainmail', 31, 0, 2),
          Item('splintmail', 53, 0, 3),
          Item('bandedmail', 75, 0, 4),
          Item('platemail', 102, 0, 5)
        ]
          
rings = [
        Item('none', 0, 0, 0),
        Item('damage+1', 25, 1, 0),
        Item('damage+2', 50, 2, 0),
        Item('damage+3', 100, 3, 0),
        Item('defense+1', 20, 0, 1),
        Item('defense+2', 40, 0, 2),
        Item('defense+3', 80, 0, 3)
        ]


class Character(object):
    
    def __init__(self, hp):
        self.weapon = None
        self.armor = None
        self.left_ring = None
        self.right_ring = None
        self.hp = hp
        
        self.attack_score = None # set these for bosses to override gear
        self.defense_score = None
    
    @property
    def attack(self):
        if self.attack_score is None:
            score = 0
            if self.weapon is not None:
                score += self.weapon.attack
            if self.left_ring is not None:
                score += self.left_ring.attack
            if self.right_ring is not None:
                score += self.right_ring.attack
            self.attack_score = score
        return self.attack_score
    
    @property
    def defense(self):
        if self.defense_score is None:
            score = 0
            if self.armor is not None:
                score += self.armor.defense
            if self.left_ring is not None:
                score += self.left_ring.defense
            if self.right_ring is not None:
                score += self.right_ring.defense
            self.defense_score = score
        return self.defense_score
    
    @property
    def cost(self):
        cost = 0
        if self.weapon is not None:
            cost += self.weapon.cost
        if self.armor is not None:
            cost += self.armor.cost
        if self.left_ring is not None:
            cost += self.left_ring.cost
        if self.right_ring is not None:
            cost += self.right_ring.cost
        return cost


def fight(char1, char2):
    while True:
        damage = char1.attack - char2.defense
        if damage <= 0:
            return False # char 1 cannot hurt char 2
        char2.hp -= damage
        if char2.hp <= 0:
            return True # char 1 wins
        
        damage = char2.attack - char1.defense
        char1.hp -= damage
        if char1.hp <= 0:
            return False # char 2 dies

# quick tests
test_char_1 = Character(500)
test_char_1.attack_score = 50
test_char_1.defense_score = 50
test_char_2 = Character(5)
test_char_2.attack_score = 5
test_char_2.defense_score = 5

#For example, suppose you have 8 hit points, 5 damage, and 5 armor, and that the boss has 12 hit points, 7 damage, and 2 armor:
player = Character(8)
player.attack_score = 5
player.defense_score = 5

boss = Character(12)
boss.attack_score = 7
boss.defense_score = 2

assert fight(player, boss) == True # player just barely wins
assert player.hp == 2
assert boss.hp == 0


# Gear permutation



assert fight(test_char_1, test_char_2) == True

test_char_2 = Character(500)
test_char_2.attack_score = 500
test_char_2.defense_score = 0
assert fight(test_char_1, test_char_2) == False


winners = []
losers = []
for weapon in weapons:
    for armor in armors:
        for lring in rings:
            for rring in rings:
                
                char = Character(100)
                char.weapon = weapon
                char.armor = armor
                char.left_ring = lring
                char.right_ring = rring

                boss = Character(100)
                boss.attack_score = 8
                boss.defense_score = 2

                if fight(char, boss):
                    winners.append(char)
                else:
                    losers.append(char)


winners.sort(key=lambda c: c.cost, reverse=True)
print "Cheapest 5 winners:"
for winner in winners[-5:]:
    print winner.cost, winner.weapon.name, winner.armor.name, winner.left_ring.name, winner.right_ring.name

print "Most expensive 5 losers:"
losers.sort(key=lambda c: c.cost)
for loser in losers[-5:]:
    print loser.cost, loser.weapon.name, loser.armor.name, loser.left_ring.name, loser.right_ring.name


