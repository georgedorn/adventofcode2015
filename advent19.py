from collections import namedtuple
from itertools import combinations_with_replacement, permutations
import random
Replacement = namedtuple('Replacement', ['input','output'])


def get_distinct_results(replacement, molecule):
    """
    Returns all distinct molecules after having one replacement applied.
    """
    res = set()
    parts = molecule.split(replacement.input)
    for i in range(1, len(parts)):
        new_molecule = replacement.input.join(parts[:i]) + replacement.output + replacement.input.join(parts[i:])
        res.add(new_molecule)
    return res

def get_all_results(replacements, molecule):
    """
    Performs get_distinct_results for a list of replacement possibilities,
    returning a distinct set.
    """
    res = set()
    for r in replacements:
        res.update(get_distinct_results(r, molecule))
    return res

def get_one_random_result(replacement, molecule):
    return random.choice(list(get_distinct_results(replacement, molecule)))

#test
test_replacement = Replacement('H', 'OH')
test_results = get_distinct_results(test_replacement, 'HOH')
assert test_results == set(['HOOH', 'OHOH'])

test_replacements = [test_replacement]
test_replacements.append(Replacement('H', 'HO'))
test_replacements.append(Replacement('O', 'HH'))

test_results = get_all_results(test_replacements, 'HOH')
assert test_results == set(['HOHO', 'HOOH', 'HHHH', 'OHOH'])
assert len(test_results) == 4  # redundant

    
# Puzzle data
puzzle_input = """Al => ThF
Al => ThRnFAr
B => BCa
B => TiB
B => TiRnFAr
Ca => CaCa
Ca => PB
Ca => PRnFAr
Ca => SiRnFYFAr
Ca => SiRnMgAr
Ca => SiTh
F => CaF
F => PMg
F => SiAl
H => CRnAlAr
H => CRnFYFYFAr
H => CRnFYMgAr
H => CRnMgYFAr
H => HCa
H => NRnFYFAr
H => NRnMgAr
H => NTh
H => OB
H => ORnFAr
Mg => BF
Mg => TiMg
N => CRnFAr
N => HSi
O => CRnFYFAr
O => CRnMgAr
O => HP
O => NRnFAr
O => OTi
P => CaP
P => PTi
P => SiRnFAr
Si => CaSi
Th => ThCa
Ti => BP
Ti => TiTi
e => HF
e => NAl
e => OMg"""

puzzle_molecule = "CRnCaCaCaSiRnBPTiMgArSiRnSiRnMgArSiRnCaFArTiTiBSiThFYCaFArCaCaSiThCaPBSiThSiThCaCaPTiRnPBSiThRnFArArCaCaSiThCaSiThSiRnMgArCaPTiBPRnFArSiThCaSiRnFArBCaSiRnCaPRnFArPMgYCaFArCaPTiTiTiBPBSiThCaPTiBPBSiRnFArBPBSiRnCaFArBPRnSiRnFArRnSiRnBFArCaFArCaCaCaSiThSiThCaCaPBPTiTiRnFArCaPTiBSiAlArPBCaCaCaCaCaSiRnMgArCaSiThFArThCaSiThCaSiRnCaFYCaSiRnFYFArFArCaSiRnFYFArCaSiRnBPMgArSiThPRnFArCaSiRnFArTiRnSiRnFYFArCaSiRnBFArCaSiRnTiMgArSiThCaSiThCaFArPRnFArSiRnFArTiTiTiTiBCaCaSiRnCaCaFYFArSiThCaPTiBPTiBCaSiThSiRnMgArCaF"

replacements = []
for line in puzzle_input.split("\n"):
    i, o = line.split(" => ")
    replacements.append(Replacement(i, o))

#Part One:
print "Part One:", len(get_all_results(replacements, puzzle_molecule))



#Part Two:
# This approach fails as the problem space is simply too large to exhaust.
# See Part Two Part Two, below.
test_replacements = [
    Replacement('e', 'H'),
    Replacement('e', 'O'),
    Replacement('H', 'HO'),
    Replacement('H', 'OH'),
    Replacement('O', 'HH')
]

def make_every_recipe(replacements, recipe_size):
    """
    Returns of generator of every combination of replacements
    of a specific size.
    """
    for combo in combinations_with_replacement(replacements, recipe_size):
        for permutation in permutations(combo):
            yield permutation

def get_new_generation(replacement, inputs):
    new_generation = set()
    for molecule in inputs:
        new_generation.update(get_distinct_results(replacement, molecule))
    return new_generation

#quick test of generations
assert get_new_generation(test_replacements[0], ['e']) == set(['H'])
assert get_new_generation(test_replacements[1], ['e']) == set(['O'])
assert get_new_generation(test_replacements[0], ['e', 'eH', 'eO']) == set(['H', 'HH', 'HO'])

def run_every_combination(replacements, molecule, target_molecule, steps_size):
    """
    Run every possible series of replacements of 'steps' length.
    """
    recipes = make_every_recipe(replacements, steps_size)
    
    for recipe in recipes:
        molecules = [molecule]
        for action in recipe:
            molecules = get_new_generation(action, molecules)
            if not molecules:
                break #stop, this recipe killed the generation
            if target_molecule in molecules:
                return True
            
    return False

def get_smallest_step_size(replacements, starting_molecule, target_molecule, step_size_limit=1000):
    for i in range(1, step_size_limit):
        if run_every_combination(replacements, starting_molecule, target_molecule, i):
            return i
    return None

#test data
assert get_smallest_step_size(test_replacements, 'e', 'HOH', 4) == 3
assert get_smallest_step_size(test_replacements, 'e', 'HOHOHO', 20) == 6


#Part Two Part Two:
#Different approach to step 2: start with target string and work backwards
#and rather than trying to search the entire problem space, just pick
#random replacements, track whether they did anything, and watch for
#the shortest result
reverse_replacements = [Replacement(r.output, r.input) for r in replacements]
inputs = set([r.input for r in reverse_replacements]) # for checking to see if we've hit a dead end

def is_dead_end(target):
    for sub in inputs:
        if sub in target:
            return False
    return True

def run_random_replacements(target, limit):
    count = 0
    while target != 'e' and count < limit:
        if is_dead_end(target):
            return False
        replacement = random.choice(reverse_replacements)
        try:
            target = get_one_random_result(replacement, target)
        except IndexError:
            continue
        count += 1
            
    if target == 'e':
        return count
    return False
        

best = 99999 # just to start off
for i in range(1000):
    res = run_random_replacements(puzzle_molecule, best)
    if res and res < best:
        best = res
        print "Found shorter path,", best

print "Best result:", best
