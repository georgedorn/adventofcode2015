from hashlib import md5

def make_md5(secret, integer):
    return str(md5(secret + str(integer)).hexdigest())


secret = "ckczppom"

found = False
i = 0
while not found:
    res = make_md5(secret, i)
    if res.startswith('000000'):
        print i
        break
    i += 1
