from collections import defaultdict
from itertools import permutations

class Person(object):
    def __init__(self):
        self.rules = dict()
        self.left = None
        self.right = None
    
    def add_rule(self, person, amount):
        self.rules[person] = amount
    
    @property
    def happiness(self):
        return self.rules[self.left] + self.rules[self.right]

# dict of name -> Person
people = defaultdict(Person)

def parse_rule(line):
    parts = line.split(' ')
    subject = people[parts[0]]
    amount = int(parts[3])
    if parts[2] == 'lose':
        amount = 0 - amount
    neighbor = people[parts[-1].rstrip('.')]
    subject.add_rule(neighbor, amount)

def arrange_table(names):
    """
    Given a list of people's names, arrange the table.
    """
    for i in range(len(names)):
        name = names[i]
        person = people[names[i]]
        person.name = name
        idx_right = i - 1 #will wrap around to tail correctly
        idx_left = (i + 1) % len(names)
        left_neighbor = people[names[idx_left]]
        right_neighbor = people[names[idx_right]]
        person.right = right_neighbor
        right_neighbor.left = person
        person.left = left_neighbor
        left_neighbor.right = person

def evaluate_table():
    return sum([person.happiness for person in people.values()]) 

test_rules = """Alice would gain 54 happiness units by sitting next to Bob.
Alice would lose 79 happiness units by sitting next to Carol.
Alice would lose 2 happiness units by sitting next to David.
Bob would gain 83 happiness units by sitting next to Alice.
Bob would lose 7 happiness units by sitting next to Carol.
Bob would lose 63 happiness units by sitting next to David.
Carol would lose 62 happiness units by sitting next to Alice.
Carol would gain 60 happiness units by sitting next to Bob.
Carol would gain 55 happiness units by sitting next to David.
David would gain 46 happiness units by sitting next to Alice.
David would lose 7 happiness units by sitting next to Bob.
David would gain 41 happiness units by sitting next to Carol."""

for line in test_rules.split('\n'):
    parse_rule(line)

# Arrange the table
test_names = ['Alice', 'David', 'Carol', 'Bob']
arrange_table(test_names)
assert evaluate_table() == 330


def find_best_table():
    best = None
    best_names = None
    
    names = people.keys()
    
    for arrangement in permutations(names):
        arrange_table(arrangement)
        happiness = evaluate_table()
        if best is None or happiness > best:
            best = happiness
            best_names = arrangement
    
    return best, best_names

best, best_names = find_best_table()
print best, best_names
assert best == 330

# Can't easily assert the names, but whatevs

people = defaultdict(Person) # reset

for line in open('input13.txt'):
    line = line.strip()
    parse_rule(line)

# add myself
me = Person()
me.name = 'Me'
for person in people.values():
    person.add_rule(me, 0)
    me.add_rule(person, 0)

people['Me'] = me

print find_best_table()

    
