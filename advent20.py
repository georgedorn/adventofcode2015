from collections import defaultdict

def get_divisors(number):
    return [i for i in range(1, number+1) if number % i == 0]

def run_elves(max_house_number):
    houses = defaultdict(int)
    for elf in range(1, max_house_number+1):
        for house in range(elf, max_house_number+1, elf):
            houses[house] += elf * 10
    return houses


def get_present_count(house_number):
    results = run_elves(house_number)
    return results[house_number]
    
#tests
assert get_present_count(1) == 10
assert get_present_count(2) == 30
assert get_present_count(3) == 40
assert get_present_count(4) == 70
assert get_present_count(5) == 60
assert get_present_count(6) == 120
assert get_present_count(7) == 80
assert get_present_count(8) == 150
assert get_present_count(9) == 130



def part_one(house_limit=1000000):
    houses = run_elves(house_limit)
    for n, presents in houses.iteritems():
        if presents >= 34000000:
            return n

print "Part one:", part_one()



def run_elves_2(max_house_number):
    houses = defaultdict(int)
    for elf in range(1, max_house_number+1):
        for i in range(1, 51):
            house = elf * i
            if house > max_house_number:
                continue
            houses[house] += elf * 11
    return houses

test_houses = run_elves_2(100)

assert test_houses[1] == 11
assert test_houses[2] == 33
assert test_houses[3] == 44
assert test_houses[10] == 198 # 1 + 2 + 5 + 10

#brute force test using sum-of-divisors approach
for i in range(1, 51):
    assert test_houses[i] == 11 * sum(get_divisors(i))


def part_two(house_limit=1000000):
    houses = run_elves_2(house_limit)
    for n, presents in houses.iteritems():
        if presents >= 34000000:
            return n

print "Part two:", part_two()
