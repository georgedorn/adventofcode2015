def get_code_length(line):
    return len(line)

def get_string_length(line):
    unescaped = eval(line)  # tricky tricky
    return len(unescaped)

def escape(line):
    newline_chars = []
    for char in line:
        if char in ['\\', '"']:
            newline_chars.append('\\')
        newline_chars.append(char)
    return '"' + ''.join(newline_chars) + '"'

def get_rescaped_length(line):
    return get_code_length(escape(line))


# tests; use single quotes to create, and double-escape backslash
line = '""'  # example 1
assert get_code_length(line) == 2
assert get_string_length(line) == 0
assert get_rescaped_length(line) == 6

line = '"abc"'  # example 2
assert get_code_length(line) == 5
assert get_string_length(line) == 3
assert get_rescaped_length(line) == 9

line = '"aaa\\"aaa"'  # example 3; because we're not reading it from a file, need to escape the escape of the escape.
assert get_code_length(line) == 10
assert get_string_length(line) == 7
assert get_rescaped_length(line) == 16

line = '"\\x27"'  # example 4, double-escape backslash
assert get_code_length(line) == 6
assert get_string_length(line) == 1
assert get_rescaped_length(line) == 11


part_one_diff = 0
part_two_diff = 0
for line in open('input8.txt'):
    line = line.strip() # newlines
    cl = get_code_length(line)
    bl = get_string_length(line)
    rl = get_rescaped_length(line)
    part_one_diff += (cl - bl)
    part_two_diff += (rl - cl)

print "Part one:", part_one_diff
print "Part two:", part_two_diff
    
