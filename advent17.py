from itertools import combinations

containers = [11,
              30,
              47,
              31,
              32,
              36,
              3,
              1,
              5,
              3,
              32,
              36,
              15,
              11,
              46,
              26,
              28,
              1,
              19,
              3
            ]

total_count = 0
for group_size in range(1, len(containers)+1):
    for group in combinations(containers, group_size):
        if sum(group) == 150:
            total_count += 1

print "Part one:", total_count

for group_size in range(1, len(containers)+1):
    correct_groups = [group for group in combinations(containers, group_size)
                        if sum(group) == 150]
    if correct_groups:
        break

print "Part two:"
print "Smallest group size:", group_size
print "Count of this group size:", len(correct_groups)
