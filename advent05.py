import string

def vowels_count(s):
    vowels = 'aeiou'
    tally = 0
    for vowel in vowels:
        tally += s.count(vowel)
    return tally
    
assert vowels_count('aei') == 3

def has_double_letter(s):
    for letter in string.lowercase:
        if '%s%s' % (letter, letter) in s:
            return True
    return False

assert has_double_letter('aoeuii')
assert not has_double_letter('enote')

def has_no_bad_combos(s):
    for bad in ['ab', 'cd', 'pq', 'xy']:
        if bad in s:
            return False
    return True

assert has_no_bad_combos('acpx')
assert not has_no_bad_combos('abcdef')

nice = 0
fh = open('input5.txt')
lines = fh.readlines()
fh.close()

for line in lines:
    line = line.lower().strip()
    if vowels_count(line) < 3:
        continue
    if not has_double_letter(line):
        continue
    if not has_no_bad_combos(line):
        continue
    nice += 1
print nice


from copy import copy
def has_repeat_pair(s):
    """
    More complicated year 2 rule.
    """
    list_s = list(s)
    
    while len(list_s) > 1:
        first_letter = list_s.pop(0)
        second_letter = list_s.pop(0)
        
        test_string = ''.join(list_s)
        find_string = first_letter + second_letter
        if find_string in test_string:
            return True
        list_s.insert(0, second_letter)
    return False

        
        
assert has_repeat_pair('xyxy')
assert has_repeat_pair('aabcdefgaa')
assert not has_repeat_pair('aaa')

def has_offset_letter(s):
    """
    Find a letter that repeats one letter away.
    """
    list_s = list(s)
    for i, letter in enumerate(list_s):
        try:
            if list_s[i+2] == letter:
                return True
        except IndexError:
            return False

assert has_offset_letter('xyx')
assert has_offset_letter('abcdefeghi')
assert has_offset_letter('aaa')
assert not has_offset_letter('abcdefg')

def is_nice(s):
    return has_repeat_pair(s) and has_offset_letter(s)

assert is_nice('qjhvhtzxzqqjkmpb')
assert is_nice('xxyxx')
assert not is_nice('uurcxstgmygtbstg')
assert not is_nice('ieodomkazucvgmuy')

new_nice = 0
for line in lines:
    line = line.lower().strip()
    if is_nice(line):
        new_nice += 1
print new_nice
