def calc_area(l, w, h):
    return 2*l*w + 2*w*h + 2*h*l

def calc_slack(l, w, h):
    return min(l*w, w*h, h*l)

def calc_present(l, w, h):
  area = calc_area(l, w, h)
  slack = calc_slack(l, w, h)

  return area + slack

def get_dimensions(line):
    l, w, h = line.split('x')
    l = int(l)
    w = int(w)
    h = int(h)
    return l, w, h

def calc_ribbon(l, w, h):
    sides = [l, w, h]
    biggest_side = max(sides)
    sides.remove(biggest_side)
    wrapping = 2*sides[0] + 2*sides[1]
    bow = l * w * h
    return wrapping + bow


fh = open('input2.txt', 'r')

total = 0
count = 0
ribbon_total = 0
for line in fh:
    l, w, h = get_dimensions(line)
    count += 1
    present_total = calc_present(l, w, h)
    total += present_total
    ribbon_total += calc_ribbon(l, w, h)
    
print count, total, ribbon_total


