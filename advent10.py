import itertools

MIN_CACHE_SIZE = 1000

def cacheable_chunks(seq):
    while len(seq):
        res = []
        res.extend(seq[0:MIN_CACHE_SIZE])
        seq = seq[MIN_CACHE_SIZE:]
        while seq and res[-1] == seq[0]:
            res.append(seq.pop(0))
        yield res

cache = dict()

def say(seq):
    tuple_seq = tuple(seq)
    if tuple_seq in cache:
        return cache[tuple_seq]
    seq = list(seq)
    output = []
    while seq:
        char = seq.pop(0)
        count = 1
        while seq and seq[0] == char:
            count += 1
            seq.pop(0)
        output.append(str(count))
        output.append(str(char))
    cache[tuple_seq] = output
    return output
    
#tests
assert ''.join(say('1')) == '11'
assert ''.join(say('11')) == '21'
assert ''.join(say('21')) == '1211'
assert ''.join(say('1211')) == '111221'
assert ''.join(say('111221')) == '312211'

#start = '3113322113'
start_chunked = '3113322113'


for i in range(50):
    new_chunked = []
    for chunk in cacheable_chunks(start_chunked):
        new_chunked.extend(say(chunk))

    start_chunked = new_chunked
    print i
    
print len(start_chunked)
